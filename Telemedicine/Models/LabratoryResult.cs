﻿using System;
using System.Collections.Generic;

namespace Telemedicine.Models
{
    public partial class LabratoryResult
    {
        public int Lrid { get; set; }
        public int Lid { get; set; }
        public int Pid { get; set; }
        public string NameOfTest { get; set; }
        public DateTime DateOfTest { get; set; }
        public string Result { get; set; }
        public string ConductedBy { get; set; }
        public string DiagnosisSummary { get; set; }

        public virtual LabratoryRequest L { get; set; }
        public virtual Patient P { get; set; }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Telemedicine.Models
{
    public partial class TelemedicineContext : DbContext
    {
        public TelemedicineContext()
        {
        }

        public TelemedicineContext(DbContextOptions<TelemedicineContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<Appointment> Appointment { get; set; }
        public virtual DbSet<Doctor> Doctor { get; set; }
        public virtual DbSet<EmergencyContact> EmergencyContact { get; set; }
        public virtual DbSet<Feedback> Feedback { get; set; }
        public virtual DbSet<GeneralMedicalRecord> GeneralMedicalRecord { get; set; }
        public virtual DbSet<Hospital> Hospital { get; set; }
        public virtual DbSet<HospitalAdmin> HospitalAdmin { get; set; }
        public virtual DbSet<LabratoryRequest> LabratoryRequest { get; set; }
        public virtual DbSet<LabratoryResult> LabratoryResult { get; set; }
        public virtual DbSet<MedicalCerteficate> MedicalCerteficate { get; set; }
        public virtual DbSet<MedicalHistory> MedicalHistory { get; set; }
        public virtual DbSet<Nurse> Nurse { get; set; }
        public virtual DbSet<Patient> Patient { get; set; }
        public virtual DbSet<Referal> Referal { get; set; }
        public virtual DbSet<SystemAdmin> SystemAdmin { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Name=telemedicineDb");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Account>(entity =>
            {
                entity.HasKey(e => e.Uid);

                entity.Property(e => e.Uid)
                    .HasColumnName("UID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Appointment>(entity =>
            {
                entity.HasKey(e => e.Aid);

                entity.Property(e => e.Aid)
                    .HasColumnName("AID")
                    .ValueGeneratedNever();

                entity.Property(e => e.DateTime)
                    .HasColumnName("Date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.Did).HasColumnName("DID");

                entity.Property(e => e.Pid).HasColumnName("PID");

                entity.Property(e => e.Place)
                    .HasColumnName("place")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.D)
                    .WithMany(p => p.Appointment)
                    .HasForeignKey(d => d.Did)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Appointment_doctor");

                entity.HasOne(d => d.P)
                    .WithMany(p => p.Appointment)
                    .HasForeignKey(d => d.Pid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Appointment_patient");
            });

            modelBuilder.Entity<Doctor>(entity =>
            {
                entity.HasKey(e => e.Did);

                entity.ToTable("doctor");

                entity.Property(e => e.Did)
                    .HasColumnName("DID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Firstname)
                    .IsRequired()
                    .HasColumnName("firstname")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Hid).HasColumnName("HID");

                entity.Property(e => e.Lastname)
                    .IsRequired()
                    .HasColumnName("lastname")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone).HasColumnName("phone");

                entity.Property(e => e.Speciality)
                    .IsRequired()
                    .HasColumnName("speciality")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Uid).HasColumnName("UID");

                entity.HasOne(d => d.H)
                    .WithMany(p => p.Doctor)
                    .HasForeignKey(d => d.Hid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_doctor_hospital");

                entity.HasOne(d => d.U)
                    .WithMany(p => p.Doctor)
                    .HasForeignKey(d => d.Uid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_doctor_Account");
            });

            modelBuilder.Entity<EmergencyContact>(entity =>
            {
                entity.HasKey(e => e.Eid);

                entity.ToTable("emergency contact");

                entity.Property(e => e.Eid)
                    .HasColumnName("EID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasColumnName("address")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Firstname)
                    .IsRequired()
                    .HasColumnName("firstname")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Lastname)
                    .IsRequired()
                    .HasColumnName("lastname")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Mid).HasColumnName("MID");

                entity.Property(e => e.Phone).HasColumnName("phone");

                entity.Property(e => e.Pid).HasColumnName("PID");

                entity.Property(e => e.Relationship)
                    .IsRequired()
                    .HasColumnName("relationship")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.M)
                    .WithMany(p => p.EmergencyContact)
                    .HasForeignKey(d => d.Mid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_emergency contact_general medical record");

                entity.HasOne(d => d.P)
                    .WithMany(p => p.EmergencyContact)
                    .HasForeignKey(d => d.Pid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_emergency contact_patient");
            });

            modelBuilder.Entity<Feedback>(entity =>
            {
                entity.HasKey(e => e.Fid);

                entity.Property(e => e.Fid)
                    .HasColumnName("FID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Department)
                    .IsRequired()
                    .HasColumnName("department")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Diagnosis)
                    .IsRequired()
                    .HasColumnName("diagnosis")
                    .HasColumnType("text");

                entity.Property(e => e.Did).HasColumnName("DID");

                entity.Property(e => e.Findings)
                    .IsRequired()
                    .HasColumnName("findings")
                    .HasColumnType("text");

                entity.Property(e => e.FollowedBy)
                    .IsRequired()
                    .HasColumnName("followed_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PatientName)
                    .IsRequired()
                    .HasColumnName("patient_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pid).HasColumnName("PID");

                entity.Property(e => e.ToHospital)
                    .IsRequired()
                    .HasColumnName("to_hospital")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Treatment)
                    .IsRequired()
                    .HasColumnName("treatment")
                    .HasColumnType("text");

                entity.HasOne(d => d.D)
                    .WithMany(p => p.Feedback)
                    .HasForeignKey(d => d.Did)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Feedback_doctor");

                entity.HasOne(d => d.P)
                    .WithMany(p => p.Feedback)
                    .HasForeignKey(d => d.Pid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Feedback_patient");
            });

            modelBuilder.Entity<GeneralMedicalRecord>(entity =>
            {
                entity.HasKey(e => e.Mid);

                entity.ToTable("general medical record");

                entity.Property(e => e.Mid)
                    .HasColumnName("MID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Eid).HasColumnName("EID");

                entity.Property(e => e.Height).HasColumnName("height");

                entity.Property(e => e.Pid).HasColumnName("PID");

                entity.Property(e => e.Weight).HasColumnName("weight");

                entity.HasOne(d => d.E)
                    .WithMany(p => p.GeneralMedicalRecord)
                    .HasForeignKey(d => d.Eid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_general medical record_emergency contact");

                entity.HasOne(d => d.P)
                    .WithMany(p => p.GeneralMedicalRecord)
                    .HasForeignKey(d => d.Pid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_general medical record_patient");
            });

            modelBuilder.Entity<Hospital>(entity =>
            {
                entity.HasKey(e => e.Hid);

                entity.ToTable("hospital");

                entity.Property(e => e.Hid)
                    .HasColumnName("HID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasColumnName("address")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Hospitalname)
                    .IsRequired()
                    .HasColumnName("hospitalname")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<HospitalAdmin>(entity =>
            {
                entity.HasKey(e => e.Haid);

                entity.ToTable("hospital admin");

                entity.Property(e => e.Haid)
                    .HasColumnName("HAID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Hid).HasColumnName("HID");

                entity.Property(e => e.Uid).HasColumnName("UID");

                entity.HasOne(d => d.H)
                    .WithMany(p => p.HospitalAdmin)
                    .HasForeignKey(d => d.Hid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_hospital admin_hospital");

                entity.HasOne(d => d.U)
                    .WithMany(p => p.HospitalAdmin)
                    .HasForeignKey(d => d.Uid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_hospital admin_Account");
            });

            modelBuilder.Entity<LabratoryRequest>(entity =>
            {
                entity.HasKey(e => e.Lid);

                entity.ToTable("labratory request");

                entity.Property(e => e.Lid)
                    .HasColumnName("LID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AdditionalTests)
                    .IsRequired()
                    .HasColumnName("additional_tests")
                    .HasColumnType("text");

                entity.Property(e => e.ClinicalInfo)
                    .IsRequired()
                    .HasColumnName("clinical_info")
                    .HasColumnType("text");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("date");

                entity.Property(e => e.DateOfLastDose)
                    .HasColumnName("date_of_last_dose")
                    .HasColumnType("date");

                entity.Property(e => e.DateOfSampleTaken)
                    .HasColumnName("date_of_sample_taken")
                    .HasColumnType("date");

                entity.Property(e => e.DoctorsName)
                    .IsRequired()
                    .HasColumnName("doctors_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DrugTherapy)
                    .IsRequired()
                    .HasColumnName("drug_therapy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ExaminationRequested)
                    .IsRequired()
                    .HasColumnName("examination_requested")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Fasting).HasColumnName("fasting");

                entity.Property(e => e.Hid).HasColumnName("HID");

                entity.Property(e => e.LastDose)
                    .IsRequired()
                    .HasColumnName("last_dose")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OtherInfo)
                    .IsRequired()
                    .HasColumnName("other_info")
                    .HasColumnType("text");

                entity.Property(e => e.Pid).HasColumnName("PID");

                entity.Property(e => e.Sample)
                    .IsRequired()
                    .HasColumnName("sample")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Urgency).HasColumnName("urgency");

                entity.HasOne(d => d.H)
                    .WithMany(p => p.LabratoryRequest)
                    .HasForeignKey(d => d.Hid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_labratory request_hospital");

                entity.HasOne(d => d.P)
                    .WithMany(p => p.LabratoryRequest)
                    .HasForeignKey(d => d.Pid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_labratory request_patient");
            });

            modelBuilder.Entity<LabratoryResult>(entity =>
            {
                entity.HasKey(e => e.Lrid);

                entity.ToTable("labratory result");

                entity.Property(e => e.Lrid)
                    .HasColumnName("LRID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ConductedBy)
                    .IsRequired()
                    .HasColumnName("conducted_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DateOfTest)
                    .HasColumnName("date_of_test")
                    .HasColumnType("date");

                entity.Property(e => e.DiagnosisSummary)
                    .IsRequired()
                    .HasColumnName("diagnosis_summary")
                    .HasColumnType("text");

                entity.Property(e => e.Lid).HasColumnName("LID");

                entity.Property(e => e.NameOfTest)
                    .IsRequired()
                    .HasColumnName("name_of_test")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pid).HasColumnName("PID");

                entity.Property(e => e.Result)
                    .IsRequired()
                    .HasColumnName("result")
                    .HasColumnType("text");

                entity.HasOne(d => d.L)
                    .WithMany(p => p.LabratoryResult)
                    .HasForeignKey(d => d.Lid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_labratory result_labratory request");

                entity.HasOne(d => d.P)
                    .WithMany(p => p.LabratoryResult)
                    .HasForeignKey(d => d.Pid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_labratory result_patient");
            });

            modelBuilder.Entity<MedicalCerteficate>(entity =>
            {
                entity.HasKey(e => e.Mcid);

                entity.ToTable("Medical certeficate");

                entity.Property(e => e.Mcid)
                    .HasColumnName("MCID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Cost).HasColumnName("cost");

                entity.Property(e => e.Diagnosis)
                    .IsRequired()
                    .HasColumnName("diagnosis")
                    .HasColumnType("text");

                entity.Property(e => e.DoctorName)
                    .IsRequired()
                    .HasColumnName("doctor_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PatientName)
                    .IsRequired()
                    .HasColumnName("patient_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pid).HasColumnName("PID");

                entity.Property(e => e.Prescription)
                    .IsRequired()
                    .HasColumnName("prescription")
                    .HasColumnType("text");

                entity.HasOne(d => d.P)
                    .WithMany(p => p.MedicalCerteficate)
                    .HasForeignKey(d => d.Pid)
                    .HasConstraintName("FK_Medical certeficate_patient");
            });

            modelBuilder.Entity<MedicalHistory>(entity =>
            {
                entity.HasKey(e => e.Mhid);

                entity.ToTable("Medical history");

                entity.Property(e => e.Mhid)
                    .HasColumnName("MHID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Allergy)
                    .IsRequired()
                    .HasColumnName("allergy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Assessment)
                    .IsRequired()
                    .HasColumnName("assessment")
                    .HasColumnType("text");

                entity.Property(e => e.CurrentMedication)
                    .IsRequired()
                    .HasColumnName("current_medication")
                    .HasColumnType("text");

                entity.Property(e => e.Mid).HasColumnName("MID");

                entity.Property(e => e.PhysicalExamination)
                    .HasColumnName("physical_examination")
                    .HasColumnType("text");

                entity.Property(e => e.Pid).HasColumnName("PID");

                entity.Property(e => e.RecentOperation)
                    .IsRequired()
                    .HasColumnName("recent_operation")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.M)
                    .WithMany(p => p.MedicalHistory)
                    .HasForeignKey(d => d.Mid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Medical history_general medical record");

                entity.HasOne(d => d.P)
                    .WithMany(p => p.MedicalHistory)
                    .HasForeignKey(d => d.Pid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Medical history_patient");
            });

            modelBuilder.Entity<Nurse>(entity =>
            {
                entity.HasKey(e => e.Nid);

                entity.ToTable("nurse");

                entity.Property(e => e.Nid)
                    .HasColumnName("NID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Fname)
                    .IsRequired()
                    .HasColumnName("fname")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Hid).HasColumnName("HID");

                entity.Property(e => e.Lname)
                    .IsRequired()
                    .HasColumnName("lname")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone).HasColumnName("phone");

                entity.Property(e => e.Uid).HasColumnName("UID");

                entity.HasOne(d => d.H)
                    .WithMany(p => p.Nurse)
                    .HasForeignKey(d => d.Hid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_nurse_hospital");

                entity.HasOne(d => d.U)
                    .WithMany(p => p.Nurse)
                    .HasForeignKey(d => d.Uid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_nurse_Account");
            });

            modelBuilder.Entity<Patient>(entity =>
            {
                entity.HasKey(e => e.Pid);

                entity.ToTable("patient");

                entity.Property(e => e.Pid)
                    .HasColumnName("PID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasColumnName("address")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Birthdate)
                    .HasColumnName("birthdate")
                    .HasColumnType("date");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Firstname)
                    .IsRequired()
                    .HasColumnName("firstname")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Lastname)
                    .IsRequired()
                    .HasColumnName("lastname")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Occupation)
                    .HasColumnName("occupation")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone).HasColumnName("phone");

                entity.Property(e => e.Sex)
                    .IsRequired()
                    .HasColumnName("sex")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Uid).HasColumnName("UID");

                entity.HasOne(d => d.U)
                    .WithMany(p => p.Patient)
                    .HasForeignKey(d => d.Uid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_patient_Account");
            });

            modelBuilder.Entity<Referal>(entity =>
            {
                entity.HasKey(e => e.Rid);

                entity.ToTable("referal");

                entity.Property(e => e.Rid)
                    .HasColumnName("RID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ClinicalFinding)
                    .IsRequired()
                    .HasColumnName("clinical_finding")
                    .HasColumnType("text");

                entity.Property(e => e.Diagnosis)
                    .IsRequired()
                    .HasColumnName("diagnosis")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Hid).HasColumnName("HID");

                entity.Property(e => e.InvestigationResult)
                    .HasColumnName("investigation_result")
                    .HasColumnType("text");

                entity.Property(e => e.Pid).HasColumnName("PID");

                entity.Property(e => e.Reason)
                    .IsRequired()
                    .HasColumnName("reason")
                    .HasColumnType("text");

                entity.Property(e => e.ReferedBy)
                    .IsRequired()
                    .HasColumnName("refered_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ToHospital)
                    .IsRequired()
                    .HasColumnName("to_hospital")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.H)
                    .WithMany(p => p.Referal)
                    .HasForeignKey(d => d.Hid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_referal_hospital");

                entity.HasOne(d => d.P)
                    .WithMany(p => p.Referal)
                    .HasForeignKey(d => d.Pid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_referal_patient");
            });

            modelBuilder.Entity<SystemAdmin>(entity =>
            {
                entity.HasKey(e => e.Sid);

                entity.ToTable("system admin");

                entity.Property(e => e.Sid)
                    .HasColumnName("SID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Uid).HasColumnName("UID");

                entity.HasOne(d => d.U)
                    .WithMany(p => p.SystemAdmin)
                    .HasForeignKey(d => d.Uid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_system admin_Account");
            });
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Telemedicine.Models
{
    public partial class GeneralMedicalRecord
    {
        public GeneralMedicalRecord()
        {
            EmergencyContact = new HashSet<EmergencyContact>();
            MedicalHistory = new HashSet<MedicalHistory>();
        }

        public int Mid { get; set; }
        public int Pid { get; set; }
        public int Eid { get; set; }
        public double Weight { get; set; }
        public double Height { get; set; }

        public virtual EmergencyContact E { get; set; }
        public virtual Patient P { get; set; }
        public virtual ICollection<EmergencyContact> EmergencyContact { get; set; }
        public virtual ICollection<MedicalHistory> MedicalHistory { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Telemedicine.Models
{
    public partial class MedicalHistory
    {
        public int Mhid { get; set; }
        public int Mid { get; set; }
        public int Pid { get; set; }
        public string Allergy { get; set; }
        public string RecentOperation { get; set; }
        public string CurrentMedication { get; set; }
        public string PhysicalExamination { get; set; }
        public string Assessment { get; set; }

        public virtual GeneralMedicalRecord M { get; set; }
        public virtual Patient P { get; set; }
    }
}

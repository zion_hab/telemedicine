﻿using System;
using System.Collections.Generic;

namespace Telemedicine.Models
{
    public partial class Referal
    {
        public int Rid { get; set; }
        public int Pid { get; set; }
        public int Hid { get; set; }
        public string ToHospital { get; set; }
        public string ClinicalFinding { get; set; }
        public string Diagnosis { get; set; }
        public string InvestigationResult { get; set; }
        public string Reason { get; set; }
        public string ReferedBy { get; set; }

        public virtual Hospital H { get; set; }
        public virtual Patient P { get; set; }
    }
}

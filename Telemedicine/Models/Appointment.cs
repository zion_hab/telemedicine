﻿using System;
using System.Collections.Generic;

namespace Telemedicine.Models
{
    public partial class Appointment
    {
        public int Aid { get; set; }
        public int Pid { get; set; }
        public int Did { get; set; }
        public DateTime DateTime { get; set; }
        public string Place { get; set; }

        public virtual Doctor D { get; set; }
        public virtual Patient P { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Telemedicine.Models
{
    public partial class EmergencyContact
    {
        public EmergencyContact()
        {
            GeneralMedicalRecord = new HashSet<GeneralMedicalRecord>();
        }

        public int Eid { get; set; }
        public int Mid { get; set; }
        public int Pid { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public int Phone { get; set; }
        public string Address { get; set; }
        public string Relationship { get; set; }

        public virtual GeneralMedicalRecord M { get; set; }
        public virtual Patient P { get; set; }
        public virtual ICollection<GeneralMedicalRecord> GeneralMedicalRecord { get; set; }
    }
}

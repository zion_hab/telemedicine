﻿using System;
using System.Collections.Generic;

namespace Telemedicine.Models
{
    public partial class Hospital
    {
        public Hospital()
        {
            Doctor = new HashSet<Doctor>();
            HospitalAdmin = new HashSet<HospitalAdmin>();
            LabratoryRequest = new HashSet<LabratoryRequest>();
            Nurse = new HashSet<Nurse>();
            Referal = new HashSet<Referal>();
        }

        public int Hid { get; set; }
        public string Hospitalname { get; set; }
        public string Address { get; set; }

        public virtual ICollection<Doctor> Doctor { get; set; }
        public virtual ICollection<HospitalAdmin> HospitalAdmin { get; set; }
        public virtual ICollection<LabratoryRequest> LabratoryRequest { get; set; }
        public virtual ICollection<Nurse> Nurse { get; set; }
        public virtual ICollection<Referal> Referal { get; set; }
    }
}

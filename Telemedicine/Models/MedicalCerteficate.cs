﻿using System;
using System.Collections.Generic;

namespace Telemedicine.Models
{
    public partial class MedicalCerteficate
    {
        public int Mcid { get; set; }
        public int? Pid { get; set; }
        public string DoctorName { get; set; }
        public string PatientName { get; set; }
        public double? Cost { get; set; }
        public string Diagnosis { get; set; }
        public string Prescription { get; set; }

        public virtual Patient P { get; set; }
    }
}

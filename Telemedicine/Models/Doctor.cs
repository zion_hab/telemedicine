﻿using System;
using System.Collections.Generic;

namespace Telemedicine.Models
{
    public partial class Doctor
    {
        public Doctor()
        {
            Appointment = new HashSet<Appointment>();
            Feedback = new HashSet<Feedback>();
        }

        public int Did { get; set; }
        public int Uid { get; set; }
        public int Hid { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Speciality { get; set; }
        public int Phone { get; set; }
        public string Email { get; set; }

        public virtual Hospital H { get; set; }
        public virtual Account U { get; set; }
        public virtual ICollection<Appointment> Appointment { get; set; }
        public virtual ICollection<Feedback> Feedback { get; set; }
    }
}

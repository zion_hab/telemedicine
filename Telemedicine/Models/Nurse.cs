﻿using System;
using System.Collections.Generic;

namespace Telemedicine.Models
{
    public partial class Nurse
    {
        public int Nid { get; set; }
        public int Uid { get; set; }
        public int Hid { get; set; }
        public string Fname { get; set; }
        public string Lname { get; set; }
        public string Email { get; set; }
        public int Phone { get; set; }

        public virtual Hospital H { get; set; }
        public virtual Account U { get; set; }
    }
}

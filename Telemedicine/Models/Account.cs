﻿using System;
using System.Collections.Generic;

namespace Telemedicine.Models
{
    public partial class Account
    {
        public Account()
        {
            Doctor = new HashSet<Doctor>();
            HospitalAdmin = new HashSet<HospitalAdmin>();
            Nurse = new HashSet<Nurse>();
            Patient = new HashSet<Patient>();
            SystemAdmin = new HashSet<SystemAdmin>();
        }

        public int Uid { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public virtual ICollection<Doctor> Doctor { get; set; }
        public virtual ICollection<HospitalAdmin> HospitalAdmin { get; set; }
        public virtual ICollection<Nurse> Nurse { get; set; }
        public virtual ICollection<Patient> Patient { get; set; }
        public virtual ICollection<SystemAdmin> SystemAdmin { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Telemedicine.Models
{
    public partial class HospitalAdmin
    {
        public int Haid { get; set; }
        public int Uid { get; set; }
        public int Hid { get; set; }

        public virtual Hospital H { get; set; }
        public virtual Account U { get; set; }
    }
}

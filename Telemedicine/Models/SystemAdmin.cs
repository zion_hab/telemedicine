﻿using System;
using System.Collections.Generic;

namespace Telemedicine.Models
{
    public partial class SystemAdmin
    {
        public int Sid { get; set; }
        public int Uid { get; set; }

        public virtual Account U { get; set; }
    }
}

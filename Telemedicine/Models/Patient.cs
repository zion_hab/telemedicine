﻿using System;
using System.Collections.Generic;

namespace Telemedicine.Models
{
    public partial class Patient
    {
        public Patient()
        {
            Appointment = new HashSet<Appointment>();
            EmergencyContact = new HashSet<EmergencyContact>();
            Feedback = new HashSet<Feedback>();
            GeneralMedicalRecord = new HashSet<GeneralMedicalRecord>();
            LabratoryRequest = new HashSet<LabratoryRequest>();
            LabratoryResult = new HashSet<LabratoryResult>();
            MedicalCerteficate = new HashSet<MedicalCerteficate>();
            MedicalHistory = new HashSet<MedicalHistory>();
            Referal = new HashSet<Referal>();
        }

        public int Pid { get; set; }
        public int Uid { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Sex { get; set; }
        public DateTime Birthdate { get; set; }
        public string Address { get; set; }
        public int Phone { get; set; }
        public string Email { get; set; }
        public string Occupation { get; set; }

        public virtual Account U { get; set; }
        public virtual ICollection<Appointment> Appointment { get; set; }
        public virtual ICollection<EmergencyContact> EmergencyContact { get; set; }
        public virtual ICollection<Feedback> Feedback { get; set; }
        public virtual ICollection<GeneralMedicalRecord> GeneralMedicalRecord { get; set; }
        public virtual ICollection<LabratoryRequest> LabratoryRequest { get; set; }
        public virtual ICollection<LabratoryResult> LabratoryResult { get; set; }
        public virtual ICollection<MedicalCerteficate> MedicalCerteficate { get; set; }
        public virtual ICollection<MedicalHistory> MedicalHistory { get; set; }
        public virtual ICollection<Referal> Referal { get; set; }
    }
}

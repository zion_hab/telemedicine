﻿using System;
using System.Collections.Generic;

namespace Telemedicine.Models
{
    public partial class LabratoryRequest
    {
        public LabratoryRequest()
        {
            LabratoryResult = new HashSet<LabratoryResult>();
        }

        public int Lid { get; set; }
        public int Pid { get; set; }
        public int Hid { get; set; }
        public string DoctorsName { get; set; }
        public bool Urgency { get; set; }
        public bool Fasting { get; set; }
        public string Sample { get; set; }
        public DateTime DateOfSampleTaken { get; set; }
        public string DrugTherapy { get; set; }
        public string LastDose { get; set; }
        public DateTime DateOfLastDose { get; set; }
        public string OtherInfo { get; set; }
        public string ClinicalInfo { get; set; }
        public string ExaminationRequested { get; set; }
        public string AdditionalTests { get; set; }
        public DateTime Date { get; set; }

        public virtual Hospital H { get; set; }
        public virtual Patient P { get; set; }
        public virtual ICollection<LabratoryResult> LabratoryResult { get; set; }
    }
}

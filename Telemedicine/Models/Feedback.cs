﻿using System;
using System.Collections.Generic;

namespace Telemedicine.Models
{
    public partial class Feedback
    {
        public int Fid { get; set; }
        public int Pid { get; set; }
        public int Did { get; set; }
        public string PatientName { get; set; }
        public string ToHospital { get; set; }
        public string Department { get; set; }
        public string Findings { get; set; }
        public string Diagnosis { get; set; }
        public string Treatment { get; set; }
        public string FollowedBy { get; set; }

        public virtual Doctor D { get; set; }
        public virtual Patient P { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Telemedicine.Models;

namespace Telemedicine.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LabratoryRequestsController : ControllerBase
    {
        private readonly TelemedicineContext _context;

        public LabratoryRequestsController(TelemedicineContext context)
        {
            _context = context;
        }

        // GET: api/LabratoryRequests
        [HttpGet]
        public async Task<ActionResult<IEnumerable<LabratoryRequest>>> GetLabratoryRequest()
        {
            return await _context.LabratoryRequest.ToListAsync();
        }

        // GET: api/LabratoryRequests/5
        [HttpGet("{id}")]
        public async Task<ActionResult<LabratoryRequest>> GetLabratoryRequest(int id)
        {
            var labratoryRequest = await _context.LabratoryRequest.FindAsync(id);

            if (labratoryRequest == null)
            {
                return NotFound();
            }

            return labratoryRequest;
        }

        // PUT: api/LabratoryRequests/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLabratoryRequest(int id, LabratoryRequest labratoryRequest)
        {
            if (id != labratoryRequest.Lid)
            {
                return BadRequest();
            }

            _context.Entry(labratoryRequest).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LabratoryRequestExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/LabratoryRequests
        [HttpPost]
        public async Task<ActionResult<LabratoryRequest>> PostLabratoryRequest(LabratoryRequest labratoryRequest)
        {
            _context.LabratoryRequest.Add(labratoryRequest);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (LabratoryRequestExists(labratoryRequest.Lid))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetLabratoryRequest", new { id = labratoryRequest.Lid }, labratoryRequest);
        }

        // DELETE: api/LabratoryRequests/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<LabratoryRequest>> DeleteLabratoryRequest(int id)
        {
            var labratoryRequest = await _context.LabratoryRequest.FindAsync(id);
            if (labratoryRequest == null)
            {
                return NotFound();
            }

            _context.LabratoryRequest.Remove(labratoryRequest);
            await _context.SaveChangesAsync();

            return labratoryRequest;
        }

        private bool LabratoryRequestExists(int id)
        {
            return _context.LabratoryRequest.Any(e => e.Lid == id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Telemedicine.Models;

namespace Telemedicine.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SystemAdminsController : ControllerBase
    {
        private readonly TelemedicineContext _context;

        public SystemAdminsController(TelemedicineContext context)
        {
            _context = context;
        }

        // GET: api/SystemAdmins
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SystemAdmin>>> GetSystemAdmin()
        {
            return await _context.SystemAdmin.ToListAsync();
        }

        // GET: api/SystemAdmins/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SystemAdmin>> GetSystemAdmin(int id)
        {
            var systemAdmin = await _context.SystemAdmin.FindAsync(id);

            if (systemAdmin == null)
            {
                return NotFound();
            }

            return systemAdmin;
        }

        // PUT: api/SystemAdmins/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSystemAdmin(int id, SystemAdmin systemAdmin)
        {
            if (id != systemAdmin.Sid)
            {
                return BadRequest();
            }

            _context.Entry(systemAdmin).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SystemAdminExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SystemAdmins
        [HttpPost]
        public async Task<ActionResult<SystemAdmin>> PostSystemAdmin(SystemAdmin systemAdmin)
        {
            _context.SystemAdmin.Add(systemAdmin);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SystemAdminExists(systemAdmin.Sid))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetSystemAdmin", new { id = systemAdmin.Sid }, systemAdmin);
        }

        // DELETE: api/SystemAdmins/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<SystemAdmin>> DeleteSystemAdmin(int id)
        {
            var systemAdmin = await _context.SystemAdmin.FindAsync(id);
            if (systemAdmin == null)
            {
                return NotFound();
            }

            _context.SystemAdmin.Remove(systemAdmin);
            await _context.SaveChangesAsync();

            return systemAdmin;
        }

        private bool SystemAdminExists(int id)
        {
            return _context.SystemAdmin.Any(e => e.Sid == id);
        }
    }
}

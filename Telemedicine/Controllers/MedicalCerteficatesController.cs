﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Telemedicine.Models;

namespace Telemedicine.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MedicalCerteficatesController : ControllerBase
    {
        private readonly TelemedicineContext _context;

        public MedicalCerteficatesController(TelemedicineContext context)
        {
            _context = context;
        }

        // GET: api/MedicalCerteficates
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MedicalCerteficate>>> GetMedicalCerteficate()
        {
            return await _context.MedicalCerteficate.ToListAsync();
        }

        // GET: api/MedicalCerteficates/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MedicalCerteficate>> GetMedicalCerteficate(int id)
        {
            var medicalCerteficate = await _context.MedicalCerteficate.FindAsync(id);

            if (medicalCerteficate == null)
            {
                return NotFound();
            }

            return medicalCerteficate;
        }

        // PUT: api/MedicalCerteficates/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMedicalCerteficate(int id, MedicalCerteficate medicalCerteficate)
        {
            if (id != medicalCerteficate.Mcid)
            {
                return BadRequest();
            }

            _context.Entry(medicalCerteficate).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MedicalCerteficateExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MedicalCerteficates
        [HttpPost]
        public async Task<ActionResult<MedicalCerteficate>> PostMedicalCerteficate(MedicalCerteficate medicalCerteficate)
        {
            _context.MedicalCerteficate.Add(medicalCerteficate);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (MedicalCerteficateExists(medicalCerteficate.Mcid))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetMedicalCerteficate", new { id = medicalCerteficate.Mcid }, medicalCerteficate);
        }

        // DELETE: api/MedicalCerteficates/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<MedicalCerteficate>> DeleteMedicalCerteficate(int id)
        {
            var medicalCerteficate = await _context.MedicalCerteficate.FindAsync(id);
            if (medicalCerteficate == null)
            {
                return NotFound();
            }

            _context.MedicalCerteficate.Remove(medicalCerteficate);
            await _context.SaveChangesAsync();

            return medicalCerteficate;
        }

        private bool MedicalCerteficateExists(int id)
        {
            return _context.MedicalCerteficate.Any(e => e.Mcid == id);
        }
    }
}

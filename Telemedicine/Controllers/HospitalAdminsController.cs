﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Telemedicine.Models;

namespace Telemedicine.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HospitalAdminsController : ControllerBase
    {
        private readonly TelemedicineContext _context;

        public HospitalAdminsController(TelemedicineContext context)
        {
            _context = context;
        }

        // GET: api/HospitalAdmins
        [HttpGet]
        public async Task<ActionResult<IEnumerable<HospitalAdmin>>> GetHospitalAdmin()
        {
            return await _context.HospitalAdmin.ToListAsync();
        }

        // GET: api/HospitalAdmins/5
        [HttpGet("{id}")]
        public async Task<ActionResult<HospitalAdmin>> GetHospitalAdmin(int id)
        {
            var hospitalAdmin = await _context.HospitalAdmin.FindAsync(id);

            if (hospitalAdmin == null)
            {
                return NotFound();
            }

            return hospitalAdmin;
        }

        // PUT: api/HospitalAdmins/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutHospitalAdmin(int id, HospitalAdmin hospitalAdmin)
        {
            if (id != hospitalAdmin.Haid)
            {
                return BadRequest();
            }

            _context.Entry(hospitalAdmin).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HospitalAdminExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/HospitalAdmins
        [HttpPost]
        public async Task<ActionResult<HospitalAdmin>> PostHospitalAdmin(HospitalAdmin hospitalAdmin)
        {
            _context.HospitalAdmin.Add(hospitalAdmin);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (HospitalAdminExists(hospitalAdmin.Haid))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetHospitalAdmin", new { id = hospitalAdmin.Haid }, hospitalAdmin);
        }

        // DELETE: api/HospitalAdmins/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<HospitalAdmin>> DeleteHospitalAdmin(int id)
        {
            var hospitalAdmin = await _context.HospitalAdmin.FindAsync(id);
            if (hospitalAdmin == null)
            {
                return NotFound();
            }

            _context.HospitalAdmin.Remove(hospitalAdmin);
            await _context.SaveChangesAsync();

            return hospitalAdmin;
        }

        private bool HospitalAdminExists(int id)
        {
            return _context.HospitalAdmin.Any(e => e.Haid == id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Telemedicine.Models;

namespace Telemedicine.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReferalsController : ControllerBase
    {
        private readonly TelemedicineContext _context;

        public ReferalsController(TelemedicineContext context)
        {
            _context = context;
        }

        // GET: api/Referals
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Referal>>> GetReferal()
        {
            return await _context.Referal.ToListAsync();
        }

        // GET: api/Referals/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Referal>> GetReferal(int id)
        {
            var referal = await _context.Referal.FindAsync(id);

            if (referal == null)
            {
                return NotFound();
            }

            return referal;
        }

        // PUT: api/Referals/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutReferal(int id, Referal referal)
        {
            if (id != referal.Rid)
            {
                return BadRequest();
            }

            _context.Entry(referal).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ReferalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Referals
        [HttpPost]
        public async Task<ActionResult<Referal>> PostReferal(Referal referal)
        {
            _context.Referal.Add(referal);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ReferalExists(referal.Rid))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetReferal", new { id = referal.Rid }, referal);
        }

        // DELETE: api/Referals/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Referal>> DeleteReferal(int id)
        {
            var referal = await _context.Referal.FindAsync(id);
            if (referal == null)
            {
                return NotFound();
            }

            _context.Referal.Remove(referal);
            await _context.SaveChangesAsync();

            return referal;
        }

        private bool ReferalExists(int id)
        {
            return _context.Referal.Any(e => e.Rid == id);
        }
    }
}

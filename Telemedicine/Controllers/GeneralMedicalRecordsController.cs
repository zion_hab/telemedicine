﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Telemedicine.Models;

namespace Telemedicine.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GeneralMedicalRecordsController : ControllerBase
    {
        private readonly TelemedicineContext _context;

        public GeneralMedicalRecordsController(TelemedicineContext context)
        {
            _context = context;
        }

        // GET: api/GeneralMedicalRecords
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GeneralMedicalRecord>>> GetGeneralMedicalRecord()
        {
            return await _context.GeneralMedicalRecord.ToListAsync();
        }

        // GET: api/GeneralMedicalRecords/5
        [HttpGet("{id}")]
        public async Task<ActionResult<GeneralMedicalRecord>> GetGeneralMedicalRecord(int id)
        {
            var generalMedicalRecord = await _context.GeneralMedicalRecord.FindAsync(id);

            if (generalMedicalRecord == null)
            {
                return NotFound();
            }

            return generalMedicalRecord;
        }

        // PUT: api/GeneralMedicalRecords/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGeneralMedicalRecord(int id, GeneralMedicalRecord generalMedicalRecord)
        {
            if (id != generalMedicalRecord.Mid)
            {
                return BadRequest();
            }

            _context.Entry(generalMedicalRecord).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GeneralMedicalRecordExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/GeneralMedicalRecords
        [HttpPost]
        public async Task<ActionResult<GeneralMedicalRecord>> PostGeneralMedicalRecord(GeneralMedicalRecord generalMedicalRecord)
        {
            _context.GeneralMedicalRecord.Add(generalMedicalRecord);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (GeneralMedicalRecordExists(generalMedicalRecord.Mid))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetGeneralMedicalRecord", new { id = generalMedicalRecord.Mid }, generalMedicalRecord);
        }

        // DELETE: api/GeneralMedicalRecords/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<GeneralMedicalRecord>> DeleteGeneralMedicalRecord(int id)
        {
            var generalMedicalRecord = await _context.GeneralMedicalRecord.FindAsync(id);
            if (generalMedicalRecord == null)
            {
                return NotFound();
            }

            _context.GeneralMedicalRecord.Remove(generalMedicalRecord);
            await _context.SaveChangesAsync();

            return generalMedicalRecord;
        }

        private bool GeneralMedicalRecordExists(int id)
        {
            return _context.GeneralMedicalRecord.Any(e => e.Mid == id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Telemedicine.Models;

namespace Telemedicine.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmergencyContactsController : ControllerBase
    {
        private readonly TelemedicineContext _context;

        public EmergencyContactsController(TelemedicineContext context)
        {
            _context = context;
        }

        // GET: api/EmergencyContacts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<EmergencyContact>>> GetEmergencyContact()
        {
            return await _context.EmergencyContact.ToListAsync();
        }

        // GET: api/EmergencyContacts/5
        [HttpGet("{id}")]
        public async Task<ActionResult<EmergencyContact>> GetEmergencyContact(int id)
        {
            var emergencyContact = await _context.EmergencyContact.FindAsync(id);

            if (emergencyContact == null)
            {
                return NotFound();
            }

            return emergencyContact;
        }

        // PUT: api/EmergencyContacts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEmergencyContact(int id, EmergencyContact emergencyContact)
        {
            if (id != emergencyContact.Eid)
            {
                return BadRequest();
            }

            _context.Entry(emergencyContact).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmergencyContactExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/EmergencyContacts
        [HttpPost]
        public async Task<ActionResult<EmergencyContact>> PostEmergencyContact(EmergencyContact emergencyContact)
        {
            _context.EmergencyContact.Add(emergencyContact);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (EmergencyContactExists(emergencyContact.Eid))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetEmergencyContact", new { id = emergencyContact.Eid }, emergencyContact);
        }

        // DELETE: api/EmergencyContacts/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<EmergencyContact>> DeleteEmergencyContact(int id)
        {
            var emergencyContact = await _context.EmergencyContact.FindAsync(id);
            if (emergencyContact == null)
            {
                return NotFound();
            }

            _context.EmergencyContact.Remove(emergencyContact);
            await _context.SaveChangesAsync();

            return emergencyContact;
        }

        private bool EmergencyContactExists(int id)
        {
            return _context.EmergencyContact.Any(e => e.Eid == id);
        }
    }
}

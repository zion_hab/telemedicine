﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Telemedicine.Models;

namespace Telemedicine.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LabratoryResultsController : ControllerBase
    {
        private readonly TelemedicineContext _context;

        public LabratoryResultsController(TelemedicineContext context)
        {
            _context = context;
        }

        // GET: api/LabratoryResults
        [HttpGet]
        public async Task<ActionResult<IEnumerable<LabratoryResult>>> GetLabratoryResult()
        {
            return await _context.LabratoryResult.ToListAsync();
        }

        // GET: api/LabratoryResults/5
        [HttpGet("{id}")]
        public async Task<ActionResult<LabratoryResult>> GetLabratoryResult(int id)
        {
            var labratoryResult = await _context.LabratoryResult.FindAsync(id);

            if (labratoryResult == null)
            {
                return NotFound();
            }

            return labratoryResult;
        }

        // PUT: api/LabratoryResults/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLabratoryResult(int id, LabratoryResult labratoryResult)
        {
            if (id != labratoryResult.Lrid)
            {
                return BadRequest();
            }

            _context.Entry(labratoryResult).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LabratoryResultExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/LabratoryResults
        [HttpPost]
        public async Task<ActionResult<LabratoryResult>> PostLabratoryResult(LabratoryResult labratoryResult)
        {
            _context.LabratoryResult.Add(labratoryResult);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (LabratoryResultExists(labratoryResult.Lrid))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetLabratoryResult", new { id = labratoryResult.Lrid }, labratoryResult);
        }

        // DELETE: api/LabratoryResults/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<LabratoryResult>> DeleteLabratoryResult(int id)
        {
            var labratoryResult = await _context.LabratoryResult.FindAsync(id);
            if (labratoryResult == null)
            {
                return NotFound();
            }

            _context.LabratoryResult.Remove(labratoryResult);
            await _context.SaveChangesAsync();

            return labratoryResult;
        }

        private bool LabratoryResultExists(int id)
        {
            return _context.LabratoryResult.Any(e => e.Lrid == id);
        }
    }
}

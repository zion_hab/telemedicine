﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Telemedicine.Migrations
{
    public partial class initialmigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Account",
                columns: table => new
                {
                    UID = table.Column<int>(nullable: false),
                    username = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    password = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Account", x => x.UID);
                });

            migrationBuilder.CreateTable(
                name: "hospital",
                columns: table => new
                {
                    HID = table.Column<int>(nullable: false),
                    hospitalname = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    address = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_hospital", x => x.HID);
                });

            migrationBuilder.CreateTable(
                name: "patient",
                columns: table => new
                {
                    PID = table.Column<int>(nullable: false),
                    UID = table.Column<int>(nullable: false),
                    firstname = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    lastname = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    sex = table.Column<string>(unicode: false, maxLength: 10, nullable: false),
                    birthdate = table.Column<DateTime>(type: "date", nullable: false),
                    address = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    phone = table.Column<int>(nullable: false),
                    email = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    occupation = table.Column<string>(unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_patient", x => x.PID);
                    table.ForeignKey(
                        name: "FK_patient_Account",
                        column: x => x.UID,
                        principalTable: "Account",
                        principalColumn: "UID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "system admin",
                columns: table => new
                {
                    SID = table.Column<int>(nullable: false),
                    UID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_system admin", x => x.SID);
                    table.ForeignKey(
                        name: "FK_system admin_Account",
                        column: x => x.UID,
                        principalTable: "Account",
                        principalColumn: "UID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "doctor",
                columns: table => new
                {
                    DID = table.Column<int>(nullable: false),
                    UID = table.Column<int>(nullable: false),
                    HID = table.Column<int>(nullable: false),
                    firstname = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    lastname = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    speciality = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    phone = table.Column<int>(nullable: false),
                    email = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_doctor", x => x.DID);
                    table.ForeignKey(
                        name: "FK_doctor_hospital",
                        column: x => x.HID,
                        principalTable: "hospital",
                        principalColumn: "HID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_doctor_Account",
                        column: x => x.UID,
                        principalTable: "Account",
                        principalColumn: "UID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "hospital admin",
                columns: table => new
                {
                    HAID = table.Column<int>(nullable: false),
                    UID = table.Column<int>(nullable: false),
                    HID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_hospital admin", x => x.HAID);
                    table.ForeignKey(
                        name: "FK_hospital admin_hospital",
                        column: x => x.HID,
                        principalTable: "hospital",
                        principalColumn: "HID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_hospital admin_Account",
                        column: x => x.UID,
                        principalTable: "Account",
                        principalColumn: "UID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "nurse",
                columns: table => new
                {
                    NID = table.Column<int>(nullable: false),
                    UID = table.Column<int>(nullable: false),
                    HID = table.Column<int>(nullable: false),
                    fname = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    lname = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    email = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    phone = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_nurse", x => x.NID);
                    table.ForeignKey(
                        name: "FK_nurse_hospital",
                        column: x => x.HID,
                        principalTable: "hospital",
                        principalColumn: "HID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_nurse_Account",
                        column: x => x.UID,
                        principalTable: "Account",
                        principalColumn: "UID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "labratory request",
                columns: table => new
                {
                    LID = table.Column<int>(nullable: false),
                    PID = table.Column<int>(nullable: false),
                    HID = table.Column<int>(nullable: false),
                    doctors_name = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    urgency = table.Column<bool>(nullable: false),
                    fasting = table.Column<bool>(nullable: false),
                    sample = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    date_of_sample_taken = table.Column<DateTime>(type: "date", nullable: false),
                    drug_therapy = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    last_dose = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    date_of_last_dose = table.Column<DateTime>(type: "date", nullable: false),
                    other_info = table.Column<string>(type: "text", nullable: false),
                    clinical_info = table.Column<string>(type: "text", nullable: false),
                    examination_requested = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    additional_tests = table.Column<string>(type: "text", nullable: false),
                    date = table.Column<DateTime>(type: "date", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_labratory request", x => x.LID);
                    table.ForeignKey(
                        name: "FK_labratory request_hospital",
                        column: x => x.HID,
                        principalTable: "hospital",
                        principalColumn: "HID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_labratory request_patient",
                        column: x => x.PID,
                        principalTable: "patient",
                        principalColumn: "PID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Medical certeficate",
                columns: table => new
                {
                    MCID = table.Column<int>(nullable: false),
                    PID = table.Column<int>(nullable: true),
                    doctor_name = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    patient_name = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    cost = table.Column<double>(nullable: true),
                    diagnosis = table.Column<string>(type: "text", nullable: false),
                    prescription = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Medical certeficate", x => x.MCID);
                    table.ForeignKey(
                        name: "FK_Medical certeficate_patient",
                        column: x => x.PID,
                        principalTable: "patient",
                        principalColumn: "PID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "referal",
                columns: table => new
                {
                    RID = table.Column<int>(nullable: false),
                    PID = table.Column<int>(nullable: false),
                    HID = table.Column<int>(nullable: false),
                    to_hospital = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    clinical_finding = table.Column<string>(type: "text", nullable: false),
                    diagnosis = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    investigation_result = table.Column<string>(type: "text", nullable: true),
                    reason = table.Column<string>(type: "text", nullable: false),
                    refered_by = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_referal", x => x.RID);
                    table.ForeignKey(
                        name: "FK_referal_hospital",
                        column: x => x.HID,
                        principalTable: "hospital",
                        principalColumn: "HID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_referal_patient",
                        column: x => x.PID,
                        principalTable: "patient",
                        principalColumn: "PID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Appointment",
                columns: table => new
                {
                    AID = table.Column<int>(nullable: false),
                    PID = table.Column<int>(nullable: false),
                    DID = table.Column<int>(nullable: false),
                    Date_time = table.Column<DateTime>(type: "datetime", nullable: false),
                    place = table.Column<string>(unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Appointment", x => x.AID);
                    table.ForeignKey(
                        name: "FK_Appointment_doctor",
                        column: x => x.DID,
                        principalTable: "doctor",
                        principalColumn: "DID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Appointment_patient",
                        column: x => x.PID,
                        principalTable: "patient",
                        principalColumn: "PID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Feedback",
                columns: table => new
                {
                    FID = table.Column<int>(nullable: false),
                    PID = table.Column<int>(nullable: false),
                    DID = table.Column<int>(nullable: false),
                    patient_name = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    to_hospital = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    department = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    findings = table.Column<string>(type: "text", nullable: false),
                    diagnosis = table.Column<string>(type: "text", nullable: false),
                    treatment = table.Column<string>(type: "text", nullable: false),
                    followed_by = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Feedback", x => x.FID);
                    table.ForeignKey(
                        name: "FK_Feedback_doctor",
                        column: x => x.DID,
                        principalTable: "doctor",
                        principalColumn: "DID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Feedback_patient",
                        column: x => x.PID,
                        principalTable: "patient",
                        principalColumn: "PID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "labratory result",
                columns: table => new
                {
                    LRID = table.Column<int>(nullable: false),
                    LID = table.Column<int>(nullable: false),
                    PID = table.Column<int>(nullable: false),
                    name_of_test = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    date_of_test = table.Column<DateTime>(type: "date", nullable: false),
                    result = table.Column<string>(type: "text", nullable: false),
                    conducted_by = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    diagnosis_summary = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_labratory result", x => x.LRID);
                    table.ForeignKey(
                        name: "FK_labratory result_labratory request",
                        column: x => x.LID,
                        principalTable: "labratory request",
                        principalColumn: "LID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_labratory result_patient",
                        column: x => x.PID,
                        principalTable: "patient",
                        principalColumn: "PID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "general medical record",
                columns: table => new
                {
                    MID = table.Column<int>(nullable: false),
                    PID = table.Column<int>(nullable: false),
                    EID = table.Column<int>(nullable: false),
                    weight = table.Column<double>(nullable: false),
                    height = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_general medical record", x => x.MID);
                    table.ForeignKey(
                        name: "FK_general medical record_patient",
                        column: x => x.PID,
                        principalTable: "patient",
                        principalColumn: "PID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "emergency contact",
                columns: table => new
                {
                    EID = table.Column<int>(nullable: false),
                    MID = table.Column<int>(nullable: false),
                    PID = table.Column<int>(nullable: false),
                    firstname = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    lastname = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    email = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    phone = table.Column<int>(nullable: false),
                    address = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    relationship = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_emergency contact", x => x.EID);
                    table.ForeignKey(
                        name: "FK_emergency contact_general medical record",
                        column: x => x.MID,
                        principalTable: "general medical record",
                        principalColumn: "MID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_emergency contact_patient",
                        column: x => x.PID,
                        principalTable: "patient",
                        principalColumn: "PID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Medical history",
                columns: table => new
                {
                    MHID = table.Column<int>(nullable: false),
                    MID = table.Column<int>(nullable: false),
                    PID = table.Column<int>(nullable: false),
                    allergy = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    recent_operation = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    current_medication = table.Column<string>(type: "text", nullable: false),
                    physical_examination = table.Column<string>(type: "text", nullable: true),
                    assessment = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Medical history", x => x.MHID);
                    table.ForeignKey(
                        name: "FK_Medical history_general medical record",
                        column: x => x.MID,
                        principalTable: "general medical record",
                        principalColumn: "MID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Medical history_patient",
                        column: x => x.PID,
                        principalTable: "patient",
                        principalColumn: "PID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Appointment_DID",
                table: "Appointment",
                column: "DID");

            migrationBuilder.CreateIndex(
                name: "IX_Appointment_PID",
                table: "Appointment",
                column: "PID");

            migrationBuilder.CreateIndex(
                name: "IX_doctor_HID",
                table: "doctor",
                column: "HID");

            migrationBuilder.CreateIndex(
                name: "IX_doctor_UID",
                table: "doctor",
                column: "UID");

            migrationBuilder.CreateIndex(
                name: "IX_emergency contact_MID",
                table: "emergency contact",
                column: "MID");

            migrationBuilder.CreateIndex(
                name: "IX_emergency contact_PID",
                table: "emergency contact",
                column: "PID");

            migrationBuilder.CreateIndex(
                name: "IX_Feedback_DID",
                table: "Feedback",
                column: "DID");

            migrationBuilder.CreateIndex(
                name: "IX_Feedback_PID",
                table: "Feedback",
                column: "PID");

            migrationBuilder.CreateIndex(
                name: "IX_general medical record_EID",
                table: "general medical record",
                column: "EID");

            migrationBuilder.CreateIndex(
                name: "IX_general medical record_PID",
                table: "general medical record",
                column: "PID");

            migrationBuilder.CreateIndex(
                name: "IX_hospital admin_HID",
                table: "hospital admin",
                column: "HID");

            migrationBuilder.CreateIndex(
                name: "IX_hospital admin_UID",
                table: "hospital admin",
                column: "UID");

            migrationBuilder.CreateIndex(
                name: "IX_labratory request_HID",
                table: "labratory request",
                column: "HID");

            migrationBuilder.CreateIndex(
                name: "IX_labratory request_PID",
                table: "labratory request",
                column: "PID");

            migrationBuilder.CreateIndex(
                name: "IX_labratory result_LID",
                table: "labratory result",
                column: "LID");

            migrationBuilder.CreateIndex(
                name: "IX_labratory result_PID",
                table: "labratory result",
                column: "PID");

            migrationBuilder.CreateIndex(
                name: "IX_Medical certeficate_PID",
                table: "Medical certeficate",
                column: "PID");

            migrationBuilder.CreateIndex(
                name: "IX_Medical history_MID",
                table: "Medical history",
                column: "MID");

            migrationBuilder.CreateIndex(
                name: "IX_Medical history_PID",
                table: "Medical history",
                column: "PID");

            migrationBuilder.CreateIndex(
                name: "IX_nurse_HID",
                table: "nurse",
                column: "HID");

            migrationBuilder.CreateIndex(
                name: "IX_nurse_UID",
                table: "nurse",
                column: "UID");

            migrationBuilder.CreateIndex(
                name: "IX_patient_UID",
                table: "patient",
                column: "UID");

            migrationBuilder.CreateIndex(
                name: "IX_referal_HID",
                table: "referal",
                column: "HID");

            migrationBuilder.CreateIndex(
                name: "IX_referal_PID",
                table: "referal",
                column: "PID");

            migrationBuilder.CreateIndex(
                name: "IX_system admin_UID",
                table: "system admin",
                column: "UID");

            migrationBuilder.AddForeignKey(
                name: "FK_general medical record_emergency contact",
                table: "general medical record",
                column: "EID",
                principalTable: "emergency contact",
                principalColumn: "EID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_emergency contact_patient",
                table: "emergency contact");

            migrationBuilder.DropForeignKey(
                name: "FK_general medical record_patient",
                table: "general medical record");

            migrationBuilder.DropForeignKey(
                name: "FK_emergency contact_general medical record",
                table: "emergency contact");

            migrationBuilder.DropTable(
                name: "Appointment");

            migrationBuilder.DropTable(
                name: "Feedback");

            migrationBuilder.DropTable(
                name: "hospital admin");

            migrationBuilder.DropTable(
                name: "labratory result");

            migrationBuilder.DropTable(
                name: "Medical certeficate");

            migrationBuilder.DropTable(
                name: "Medical history");

            migrationBuilder.DropTable(
                name: "nurse");

            migrationBuilder.DropTable(
                name: "referal");

            migrationBuilder.DropTable(
                name: "system admin");

            migrationBuilder.DropTable(
                name: "doctor");

            migrationBuilder.DropTable(
                name: "labratory request");

            migrationBuilder.DropTable(
                name: "hospital");

            migrationBuilder.DropTable(
                name: "patient");

            migrationBuilder.DropTable(
                name: "Account");

            migrationBuilder.DropTable(
                name: "general medical record");

            migrationBuilder.DropTable(
                name: "emergency contact");
        }
    }
}
